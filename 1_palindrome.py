#!/usr/bin/env python3
import sys

class Palindrome():
    
    def isReverse(self, string):
        return string[::-1]

    def isPalindrome(self, string):
        if(not isinstance(string, str)):
            print("Input is not string")
            return False
        return string == self.isReverse(string)

if __name__ == "__main__": 
    palindrome = Palindrome()
    print(palindrome.isPalindrome(sys.argv[1])) 