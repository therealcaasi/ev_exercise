#!/usr/bin/env python3

def argumentType(data):
    return True if isinstance(data, list) or isinstance(data,tuple) or isinstance(data,dict) else False

if __name__ == "__main__": 
    val = {'key':'value'}
    print(argumentType(val))
    val = ('element1','element2')
    print(argumentType(val))
    val = ['element1','element2']
    print(argumentType(val))
    val = 12
    print(argumentType(val))