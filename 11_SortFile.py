#!/usr/bin/env python3

def sort(path):
    
    file = open(path, "r")

    with open("ga_sorted.txt", "w+") as outputfile:
        lines = file.readlines()
        lines.sort()
        for line in lines:
            if not '\n' in line:
                line+='\n'
            outputfile.write(line)

    file.close()

if __name__ == "__main__":
    sort("ga.txt")