#!/usr/bin/env python3

def vowelCounts(path):
    vowel_counts = {}
    
    with open(path, "r") as f:
        contents = f.read()

        for vowel in "aeiou":
            count = contents.lower().count(vowel)
            vowel_counts[vowel] = count
    
    return vowel_counts


if __name__ == "__main__":
    print(vowelCounts('ga.txt'))