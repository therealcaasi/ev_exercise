#!/usr/bin/env python3
from datetime import datetime, timedelta

if __name__ == "__main__": 
    date = datetime.now() + timedelta(weeks=52.1429, days=28, hours=4, minutes=23, seconds=32)
    
    print(date.strftime("%d-%m-%y %H:%M:%S"))