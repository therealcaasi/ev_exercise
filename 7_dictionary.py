#!/usr/bin/env python3

def createDictionary(dictionary):
    return {'True': [key for key, values in n.items() if not values], 'False': [key for key, values in n.items() if values]}

if __name__ == "__main__": 
    n = {0: {}, 1: {}, 2: {}, 3: {'a': {}, 'c': {}, 'b': {}, 'e': {}, 'd': {}, 'g': True, 'f': {}},
         4: {}, 5: {}, 6: {}, 7: {'a': True, 'c': {}, 'b': {}, 'e': {}, 'd':{}, 'g': {}, 'f': {}}, 
         8: {}, 9: {}}
    
    print(createDictionary(n))