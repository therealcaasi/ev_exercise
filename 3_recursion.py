#!/usr/bin/env python3
import sys

def sumOfAllElements(arg):
    if len(arg) == 0:
        return 0
    else:
        return arg[0] + sumOfAllElements(arg[1:])

def ise(n):
    if n == 0: return True
    else: return iso(n-1)

def iso(n):
    if n ==0: return False
    else: return ise(n-1)

if __name__ == "__main__": 
    print(sumOfAllElements([1,2,3,4,5,6,7,8,9,10]))
    print("Answer for 3.B")
    print(True)
    print(False)
    print(False)
    print(True)