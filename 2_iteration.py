#!/usr/bin/env python3
import sys

def sumOfIntergers(size):

    if (isinstance(size, int)):
        print("Size is invalid")
        return 0

    if (size < 0):
        print("Size is negative")
        return 0

    sum = 0
    for i in range(0, size+1):
        sum += i
    
    return sum

def sumOfIntegerList(integerList):

    if (not isinstance(integerList, list)):
        print("Input is not list/array")
        return 0

    sum = 0
    for i in integerList:
        sum += i
    return sum
# i = 20
# while (i > 0):
#     print("i = ",i)
#     i=-1

# def convertedWhileLoop()

if __name__ == "__main__": 
    print(sumOfIntergers(10))
    print(sumOfIntegerList([1,2,3,4,5,6,7,8,9,10]))
