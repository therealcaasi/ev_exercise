#!/usr/bin/env python3

def intersection(list1, list2): 
    intersectionDict = {}
    intersectionDict['intersecting_elements'] = {value for value in list1 if value in list2}
    intersectionDict['count'] = len(intersectionDict['intersecting_elements'])
    return intersectionDict

if __name__ == "__main__": 
    print(intersection([1,2,3,4,5],[4,5,6,7,8,9,10]))

